const YeeDevice = require('yeelight-platform').Device

const device = new YeeDevice({host: "192.168.1.11", port: 55443})
device.connect()

function turn (on = 0) {
    device.sendCommand({
          id: -1,
          method: 'set_power',
          params: [on === 0 ? "off" : "on", 'smooth', 300]
      })

  /*device.on('deviceUpdate', (newProps) => {
      console.log(newProps)
  })*/
  
    /*device.on('connected', () => {
      device.sendCommand({
          id: -1,
          method: 'set_power',
          params: [on === 0 ? "off" : "on", 'smooth', 300]
      })
  })  */
}

let isOn = false;
setInterval(() => {
  isOn === false ? turn(1) : turn(0)
  isOn = !isOn;
}, 5000);


